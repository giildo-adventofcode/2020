import dayFunction from './days/day9.2.js'

let start = (new Date()).getTime()
let response = dayFunction()
console.log(response)
let end = (new Date()).getTime()
console.log(`${end - start}ms`)
