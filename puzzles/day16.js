import fs from 'fs'
import path from 'path'

export const readMyTicket = () => {
  return fs.readFileSync(path.join(process.cwd(), '/data/day16_myTicket.txt'), { encoding: 'utf8' })
           .split(',')
           .filter(number => number !== '')
           .map(number => parseInt(number))
}

export const readNearbyTicket = () => {
  return fs.readFileSync(path.join(process.cwd(), '/data/day16_nearbyTicket.txt'), { encoding: 'utf8' })
           .split('\n')
           .map(numberRow => {
             return numberRow.split(',')
                             .filter(number => number !== '')
                             .map(number => {
                               return parseInt(number)
                             })
           })
}

export const readValuesValid = () => {
  return fs.readFileSync(path.join(process.cwd(), '/data/day16_valuesValid.txt'), { encoding: 'utf8' })
           .split('\n')
           .filter(row => row !== '')
           .map(row => {
             const {
               type,
               oneFirst,
               oneSecond,
               twoFirst,
               twoSecond,
             } = /^(?<type>[a-z ]+): (?<oneFirst>[0-9]{2})-(?<oneSecond>[0-9]{2,3}) or (?<twoFirst>[0-9]{3})-(?<twoSecond>[0-9]{3})$/.exec(row).groups

             return {
               type,
               one: [oneFirst, oneSecond],
               two: [twoFirst, twoSecond],
             }
           })
}
