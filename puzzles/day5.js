import fs from 'fs'
import path from 'path'

const readData = () => {
  return fs.readFileSync(path.join(process.cwd(), '/data/day5.txt'), { encoding: 'utf8' })
           .split('\n')
}

export default readData()
