import fs from 'fs'
import path from 'path'

// TODO: logic to join the objects
const readData = () => {
  return fs.readFileSync(path.join(process.cwd(), '/data/day4.txt'), { encoding: 'utf8' })
           .split('\n')
}

export default readData()
