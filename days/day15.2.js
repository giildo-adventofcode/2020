export default numberToFind => {
  let numbersList = [8, 13, 1, 0, 18, 9]
  let i = 1
  let numberLastIndex = new Map()
  let lastNumber = 0
  numbersList.forEach(number => {
    numberLastIndex.set(number, i)
    lastNumber = number
    i++
  })

  const nextNumber = size => {
    while (i <= size) {
      let newNumber

      if (numberLastIndex.has(lastNumber)) {
        newNumber = i - numberLastIndex.get(lastNumber) - 1
      } else {
        newNumber = 0
      }

      numberLastIndex.set(lastNumber, i - 1)
      lastNumber = newNumber
      i++
    }
  }

  nextNumber(30000000)
  return lastNumber
}
