import passwords from '../puzzles/day2.js'

export default () => {
  let passwordCorrect = 0
  passwords.forEach(password => {
    const matches = /^(?<pos1>[\d]{1,5})-(?<pos2>[\d]{1,5}) (?<char>[a-z]): (?<password>[a-z]+)$/.exec(password)
    if (matches !== null) {
      let { pos1, pos2, char, password: pass } = matches.groups
      if ((pass[pos1 - 1] === char && pass[pos2 - 1] !== char) || (pass[pos1 - 1] !== char && pass[pos2 - 1] === char)) {
        passwordCorrect++
      }
    }
  })

  return passwordCorrect
}
