import answers from '../puzzles/day6.js'

export default () => {
  return answers.reduce((previousValue, currentValue) => {
    if (currentValue === '') {
      previousValue.push({ answers: [], groupNumber: 0 })
    } else {
      const i = previousValue.length - 1
      previousValue[i].answers = [...previousValue[i].answers, ...currentValue]
      previousValue[i].groupNumber++
    }

    return previousValue
  }, [{ answers: [], groupNumber: 0 }])
                .reduce((previousNumber, answer) => {
                  let valueList = []
                  let allAnswer = 0

                  answer.answers.forEach(value => {
                    let valueObj = valueList.find(valueObj => valueObj.value === value)

                    valueObj === undefined
                    ? valueList.push({ value, number: 1 })
                    : valueObj.number++
                  })

                  valueList.forEach(value => {
                    if (value.number === answer.groupNumber) {
                      allAnswer++
                    }
                  })

                  return previousNumber + allAnswer
                }, 0)
}
