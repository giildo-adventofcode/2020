import maps from '../puzzles/day3.js'

export default patterns => {
  let treeNumbers = []

  const checkTreeNumber = pattern => {
    let col = 0
    let treeNumber = 0

    maps.forEach((row, index) => {
      if (index % parseInt(pattern[1]) === 0) {
        if (row[col] === '#') {
          treeNumber++
        }
        col = col + pattern[0]
        if ((col + 1) > row.length) {
          col = col - row.length
        }
      }
    })

    treeNumbers.push(treeNumber)
  }

  patterns.forEach(pattern => {
    checkTreeNumber(pattern)
  })

  return treeNumbers.reduce((previousValue, currentValue) => {
    return previousValue * currentValue
  }, 1)
}
