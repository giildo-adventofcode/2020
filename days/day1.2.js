import expenseReport from '../puzzles/day1.js'

export default () => {
  let expenseReport1 = null
  let expenseReport2 = null
  let expenseReport3 = null

  for (let i = 0; i < expenseReport.length; i++) {
    for (let j = i + 1; j < expenseReport.length; j++) {
      for (let k = j + 1; k < expenseReport.length; k++) {
        if ((expenseReport[i] + expenseReport[j] + expenseReport[k]) === 2020) {
          expenseReport1 = expenseReport[i]
          expenseReport2 = expenseReport[j]
          expenseReport3 = expenseReport[k]
        }
      }
    }
  }

  return expenseReport1 * expenseReport2 * expenseReport3
}
