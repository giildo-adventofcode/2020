import answers from '../puzzles/day6.js'

export default () => {
  return answers.reduce((previousValue, currentValue) => {
    if (currentValue === '') {
      previousValue.push({ answers: [], groupNumber: 0 })
    } else {
      const i = previousValue.length - 1
      for (const char of currentValue) {
        const i = previousValue.length - 1
        if (!previousValue[i].answers.includes(char)) {
          previousValue[i].answers.push(char)
        }
      }
      previousValue[i].groupNumber++
    }

    return previousValue
  }, [{ answers: [], groupNumber: 0 }])
                .reduce((previousNumber, answer) => {
                  return previousNumber + answer.answers.length
                }, 0)
}
