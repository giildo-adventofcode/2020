import rules from '../puzzles/day7.js'

export default () => {
  const checkColor = (colorTest, colorList) => {
    let newColor = false
    rules.forEach(match => {
      if (match.contain.some(matchContain => matchContain.color === colorTest) && !colorList.includes(match.color)) {
        colorList.push(match.color)
        newColor = true
      }
    })

    if (newColor) {
      colorList.forEach(subMatch => {
        checkColor(subMatch, colorList)
      })
    }

    return colorList
  }

  let colorMatches = []
  checkColor('shiny gold', colorMatches)

  return colorMatches.length
}
