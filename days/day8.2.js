import instructions from '../puzzles/day8.js'

let ids = []
let pile = []
let acc = 0
let i = 0
let finish = false
let instructionsLocale = [...instructions]

const readInstructions = () => {
  const instruction = instructionsLocale[i]

  console.log(instruction)
  if (instruction === undefined) {
    return 'acc : ' + acc
  } else if (ids.includes(instruction.id)) {
    finish = true
    return pile.pop()
  } else {
    ids.push(instruction.id)

    switch (instruction.instruction) {
      case 'acc':
        acc += instruction.value
        i++
        return readInstructions()

      case 'nop':
        if (!finish) {
          pile.push(instruction.id)
        }
        i++
        return readInstructions()

      case 'jmp':
        if (!finish) {
          pile.push(instruction.id)
        }
        i += instruction.value
        return readInstructions()
    }
  }
}

const reader = () => {
  const response = readInstructions()
  console.log(response)
  if (typeof response === 'number') {
    instructionsLocale = [...instructions.map(instruction => {
      if (instruction.id === response) {
        instruction.instruction = instruction.instruction === 'jmp' ? 'nop' : 'jmp'
      }

      return instruction
    })]
    ids.splice(0, ids.length)
    acc = 0
    i = 0
    return reader()
  }

  return response
}

export default () => {
  return reader()
}
