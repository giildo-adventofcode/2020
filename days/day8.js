import instructions from '../puzzles/day8.js'

let ids = []
let acc = 0
let i = 0

const readInstructions = () => {
  const instruction = instructions[i]

  if (ids.includes(instruction.id)) {
    return acc
  } else {
    ids.push(instruction.id)

    switch (instruction.instruction) {
      case 'acc':
        acc += instruction.value
        i++
        return readInstructions()

      case 'nop':
        i++
        return readInstructions()

      case 'jmp':
        i += instruction.value
        return readInstructions()
    }
  }
}

export default () => {
  return readInstructions()
}
