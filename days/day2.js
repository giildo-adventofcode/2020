import passwords from '../puzzles/day2.js'

export default () => {
  let passwordCorrect = 0
  passwords.forEach(password => {
    const matches = /^(?<rep1>[\d]{1,5})-(?<rep2>[\d]{1,5}) (?<char>[a-z]): (?<password>[a-z]+)$/.exec(password)
    if (matches !== null) {
      const { rep1, rep2, char, password: pass } = matches.groups
      const match = pass.match(new RegExp(char, 'g'))
      if (match && match.length >= parseInt(rep1) && match.length <= parseInt(rep2)) {
        passwordCorrect++
      }
    }
  })

  return passwordCorrect
}
