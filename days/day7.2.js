import rules from '../puzzles/day7.js'

export default () => {
  let countBag = 0
  const colorContain = (colorTest, multiply) => {
    const colorSelected = rules.find(match => match.color === colorTest)

    if (colorSelected) {
      colorSelected.contain.forEach(bagContained => {
        countBag += (parseInt(bagContained.number) * multiply)
        colorContain(bagContained.color, (parseInt(bagContained.number) * multiply))
      })
    }
  }
  colorContain('shiny gold', 1)

  return countBag
}
