import { readNearbyTicket, readValuesValid } from '../puzzles/day16.js'

const nearbyTickets = readNearbyTicket()
const valuesValid = readValuesValid()
let invalidValues = []

nearbyTickets.forEach(ticket => {
  ticket.forEach(ticketValue => {
    if (!valuesValid.some(validValue => {
      return (
        ticketValue >= validValue.one[0] &&
        ticketValue <= validValue.one[1]
      ) || (
        ticketValue >= validValue.two[0] &&
        ticketValue <= validValue.two[1]
      )
    })) {
      invalidValues.push(ticketValue)
    }
  })
})

export default () => {
  return invalidValues.reduce((previousValue, currentValue) => previousValue += currentValue)
}
