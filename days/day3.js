import maps from '../puzzles/day3.js'

export default () => {
  let col = 0
  let treeNumber = 0
  maps.forEach(row => {
    if (row[col] === '#') {
      treeNumber++
    }
    col = col + 3
    if ((col + 1) > row.length) {
      col = col - row.length
    }
  })

  return treeNumber
}
