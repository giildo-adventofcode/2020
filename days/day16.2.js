import { readNearbyTicket, readValuesValid, readMyTicket } from '../puzzles/day16.js'

let nearbyTickets = readNearbyTicket()
const valuesValid = readValuesValid()
const myTicket = readMyTicket()
let invalidValues = []
let linkTicketValueValidValue = new Map()
const listTypeValidValue = []

valuesValid.forEach(validValue => {
  listTypeValidValue.push(validValue.type)
})

myTicket.forEach((value, index) => {
  linkTicketValueValidValue.set(index, [...listTypeValidValue])
})

nearbyTickets.forEach((ticket, ticketIndex) => {
  ticket.forEach(ticketValue => {
    if (!valuesValid.some(validValue => {
      return (
        ticketValue >= validValue.one[0] &&
        ticketValue <= validValue.one[1]
      ) || (
        ticketValue >= validValue.two[0] &&
        ticketValue <= validValue.two[1]
      )
    })) {
      if (!invalidValues.includes(ticketIndex)) {
        invalidValues.push(ticketIndex)
      }
    }
  })
})

nearbyTickets = nearbyTickets.filter((ticket, index) => !invalidValues.includes(index))

nearbyTickets.forEach(ticket => {
  ticket.forEach((ticketValue, ticketValueIndex) => {
    valuesValid.forEach(validValue => {
      if (!((
        ticketValue >= validValue.one[0] &&
        ticketValue <= validValue.one[1]
      ) || (
        ticketValue >= validValue.two[0] &&
        ticketValue <= validValue.two[1]
      ))) {
        let link = linkTicketValueValidValue.get(ticketValueIndex)
        if (link.includes(validValue.type)) {
          linkTicketValueValidValue.set(ticketValueIndex, link.filter(type => type !== validValue.type))
        }
      }
    })
  })
})

const cleanMap = () => {
  let allValueUnique = true

  linkTicketValueValidValue.forEach((value, key, map) => {
    if (value.length === 1) {
      map.forEach((subValue, subKey, subMap) => {
        if (subKey !== key) {
          subMap.set(subKey, subValue.filter(oldValue => oldValue !== value[0]))
        }

        if (subMap.get(subKey).length !== 1) {
          allValueUnique = false
        }
      })
    }
  })

  if (!allValueUnique) {
    cleanMap()
  }
}

const checkDepartures = () => {
  let departuresList = []

  linkTicketValueValidValue.forEach((value, key) => {
    if (/^departure [a-z]+$/.test(value[0])) {
      departuresList.push(key)
    }
  })

  return myTicket.reduce((previousValue, currentValue, currentIndex) => {
    if (departuresList.includes(currentIndex)) {
      return previousValue * currentValue
    }

    return previousValue
  }, 1)
}

cleanMap()

export default () => {
  return checkDepartures()
}
