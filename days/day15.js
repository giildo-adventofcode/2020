export default numberToFind => {
  let numbersList = [8, 13, 1, 0, 18, 9]
  let i = numbersList.length

  const nextNumber = () => {
    let upsideDownArray = [...numbersList].reverse()
    let lastNumber = upsideDownArray.splice(0, 1)[0]
    let lastOccurrence = upsideDownArray.findIndex(number => number === lastNumber)
    if (lastOccurrence === -1) {
      numbersList.push(0)
    } else {
      numbersList.push(numbersList.length - (numbersList.length - 1 - lastOccurrence))
    }

    if (i < (numberToFind - 1)) {
      i++
      nextNumber()
    }
  }
  nextNumber()
  return numbersList[numbersList.length - 1]
}
