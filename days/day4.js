import passports from '../puzzles/day4.js'

export default () => {
  const splitCurrentValue = currentValue => {
    return currentValue.match(/(?:byr|iyr|eyr|hgt|hcl|ecl|pid|cid):[a-z0-9A-Z#]+/g)
                       .reduce((previousValue, currentValue) => {
                         const {
                           key,
                           value,
                         } = /((?<key>byr|iyr|eyr|hgt|hcl|ecl|pid|cid):(?<value>[a-z0-9A-Z#]+))/g.exec(currentValue).groups
                         previousValue[key] = value
                         return previousValue
                       }, {})
  }

  const requiredKeys = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid']

  const checkPassport = passport => requiredKeys.every(requiredKey => Object.keys(passport)
                                                                            .includes(requiredKey))

  let passportsValid = 0
  let passportsCheck = passports.reduce((previousValue, currentValue, index) => {
    if (index === 0) {
      previousValue.push(splitCurrentValue(currentValue))
      return previousValue
    } else if (currentValue !== '') {
      previousValue[previousValue.length - 1] = { ...previousValue[previousValue.length - 1], ...splitCurrentValue(currentValue) }
      return previousValue
    } else {
      if (checkPassport(previousValue[previousValue.length - 1])) passportsValid++
      previousValue.push([])
      return previousValue
    }
  }, [])

  if (checkPassport(passportsCheck[passportsCheck.length - 1])) passportsValid++

  return passportsValid
}
